import React, { Component } from 'react';
import './App.css';
import './ContactListPage.css';
import ContactsSideBar from './ContactsSideBar'
import MdContacts from 'react-icons/lib/md/contacts'
import IoChevronRight from 'react-icons/lib/io/chevron-right'

class ContactsListPage extends Component {

  openSideBarForFamily = () =>{
    document.getElementById("viewcontactsSideBarOuterIdDiv").style.display = "flex";
  }

  openSideBarForUnidentified = () =>{
    document.getElementById("contactsSideBarOuterIdDiv").style.display = "flex";
  }

  viewPhoneBook = () =>{
    this.props.history.push(`/viewphonebook`)
  }

  render() {
    return (
      <div className="contactsListPage">
          <div className="circleDiv">
            <ul>
              <li className="active"></li>
              <li className="semiActive"></li>
            </ul>  
          </div>
          <div className="popUpheaderText">
            <span className="importSpan"> Manage Family </span>
            <span className="helpSpan"> Help? </span>
          </div>
          <div className="popUpDivArea">
            <div className="popUpDiv">
                <div className="popUpHeader">
                  <span className="popUpHeaderSpan"> Choose your family member from the list of contacts you have updated </span>
                </div>
                <div className="popUpdetailsBody">
                  <div className="familyMemberDiv">
                       <div className="familyMemberCountDiv">
                          <div className="TextDiv">
                            <span> Family Members </span>
                          </div>
                          <div className="CountNumDiv">
                            <span> {this.props.familymember.length} </span>
                          </div>
                       </div>
                       <div className="familyMemberDetailsDiv">
                          <div className = "DetailText">
                            <span> Your family members are listed here. </span>
                          </div>
                          <div className = "familyDetailchooseText" onClick = {this.openSideBarForFamily}>
                            <span> Check your friends </span>
                            <IoChevronRight className="moveright"/>
                          </div>
                       </div> 
                  </div>
                  <div className="unidentifiedDiv">
                       <div className="unidentifiedCountDiv">
                          <div className="TextDiv" id="unidentifiedTextDiv">
                            <span> Unidentified </span>
                          </div>
                          <div className="CountNumDiv">
                            <span> {this.props.unidentified.length} </span>
                          </div>
                       </div>
                       <div className="unidentifiedDetailsDiv">
                          <div className = "DetailText">
                            <span> Choose your family members and their relationship to you. </span>
                          </div>
                          <div className = "UnidentifiedDetailchooseText" onClick = {this.openSideBarForUnidentified}>
                            <span> Choose from the list </span>
                            <IoChevronRight className="moveright"/>
                          </div>
                       </div> 
                    </div>
                </div>
            </div>
            <div className="phoneBookOuterDiv" onClick = {this.viewPhoneBook}>
              <div className="phoneBookIconDiv">
                <MdContacts className= "contactIconClass" />
              </div>
              <div className="phoneBookTextDiv">
                <span> View Phonebook </span>
              </div>
            </div>
          </div>
      </div>
    );
  }
}

export default ContactsListPage;