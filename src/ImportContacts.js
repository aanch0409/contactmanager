import React, { Component } from 'react';
import './App.css';
import FaFacebookSquare from 'react-icons/lib/fa/facebook-square';
import FaGoogle from 'react-icons/lib/fa/google';
import MdContactMail from 'react-icons/lib/md/contact-mail';
import FetchingContacts from './FetchingContacts'

class ImportContacts extends Component {

 googleIcononClick = () => {

    this.props.googlecontacts.map((contact) => { 
      this.props.importcontacts(contact.ContactId , contact.FirstName , contact.LastName , contact.Email , contact.PhoneNo , contact.Loc , contact.AgeRange);
    });

    this.props.history.push(`/fetching`)
 }

 facebookIcononClick = () => {

    this.props.facebookcontacts.map((contact) => { 
      this.props.importcontacts(contact.ContactId , contact.FirstName , contact.LastName , contact.Email , contact.PhoneNo , contact.Loc , contact.AgeRange);
    });

    this.props.history.push(`/fetching`)
 }

 localIcononClick = () => {

    this.props.localcontacts.map((contact) => { 
      this.props.importcontacts(contact.ContactId  , contact.FirstName , contact.LastName , contact.Email , contact.PhoneNo , contact.Loc , contact.AgeRange);
    });

    this.props.history.push(`/fetching`)
 }

  render() {
    return (
      <div className="importContactsBody">
      	    <div className="circleDiv">
      	    	<ul>
  					<li className="semiActive"></li>
  					<li className="nonActive"></li>
				</ul>  
      		</div>
      		<div className="popUpheaderText">
      			<span className="importSpan"> Import your contacts </span>
      			<span className="helpSpan"> Help? </span>
      		</div>
      		<div className="popUpDivArea">
	      		<div className="popUpDiv">
	      			  <div className="popUpHeader">
	      			  	<span className="popUpHeaderSpan"> Select a platform to import contacts </span>
	      			  </div>
	      			  <div className="popUpBody">
                        <div className="googleContacts" title = "Import google contacts">
                          <div className = "IconDiv" onClick = {this.googleIcononClick}>
                            <FaGoogle className= "svgClass"/>
                          </div>
                        </div>
                        <div className="facebookContacts" title = "Import facebook contacts">
                          <div className = "IconDiv" onClick = {this.facebookIcononClick}>
                            <FaFacebookSquare className= "svgClass"/>
                            </div>
                        </div>
                        <div className="localContacts" title = "Import local contacts">
                          <div className = "IconDiv" onClick = {this.localIcononClick}>
                            <MdContactMail className= "svgClass" />
                          </div>
                        </div>
	      			  </div>
	      		</div>
	      	</div>
      </div>
    );
  }
}

export default ImportContacts;
