import React, { Component } from 'react';
import FaPhoneSquare from 'react-icons/lib/fa/phone-square'
import IoEmail from 'react-icons/lib/io/email'
import ReactDOM from 'react-dom';
import './ViewPhoneBook.css'

class ViewPhoneBook extends Component {

  constructor(props) {
    super(props);

    this.state = {
      Contacts : [],
      UnidentifiedContact : this.props.unidentified,
      FamilyContact : this.props.familymember
    }

    this.flag=false;
  }

  filterList = (event) => {
    let updatedFamilyList = this.props.familymember;
    updatedFamilyList = updatedFamilyList.filter(({ FirstName , LastName }) => (FirstName.toLowerCase()+""+LastName.toLowerCase()).search(event.target.value.toLowerCase()) !== -1)

    let updatedUnIdentifiedList = this.props.unidentified;
    updatedUnIdentifiedList = updatedUnIdentifiedList.filter(({ FirstName , LastName}) => (FirstName.toLowerCase()+""+LastName.toLowerCase()).search(event.target.value.toLowerCase()) !== -1)

    this.flag = true;
    this.setState(
      {
        UnidentifiedContact : updatedUnIdentifiedList,
        FamilyContact : updatedFamilyList
      }
    );
  }

  MergeContacts = () =>{
    let Contact = [];
    this.props.familymember.map((contact) => { 
      let contactDetails = {
        ContactId : contact.ContactId , 
        FirstName : contact.FirstName , 
        LastName : contact.LastName ,
        Email : contact.Email ,
        AgeRange :  contact.AgeRange , 
        Loc : contact.Loc , 
        PhoneNo : contact.PhoneNo,
        Relation : contact.Relation,
        type : "F"
      }
      Contact.push(contactDetails);
    });

    this.props.unidentified.map((contact) => { 
      let contactDetails = {
        ContactId : contact.ContactId , 
        FirstName : contact.FirstName , 
        LastName : contact.LastName ,
        Email : contact.Email ,
        AgeRange :  contact.AgeRange , 
        Loc : contact.Loc , 
        PhoneNo : contact.PhoneNo,
        type : "U"
      }
      Contact.push(contactDetails);
    });

    this.setState(
      {
        Contacts: Contact
      }
    )
  }

  shouldComponentUpdate(nextProps, nextState) {
    return ((this.state.UnidentifiedContact != nextProps.unidentified) || (this.state.FamilyContact != nextProps.familymember));
  }

  componentDidUpdate()
  {
    if(this.props.flag == false)
    {
        this.setState(
        {
            UnidentifiedContact : this.props.unidentified,
            FamilyContact : this.props.familymember
        }
      )
    }
    else
    {
      this.flag = false;
    }
  }

  returnFamilyElements = () => {

    let HTML =[];

    if(this.state.FamilyContact.length > 0)
    {
      this.state.FamilyContact.map((item) => {
                HTML.push(<li className="view-list-group-item" data-category={item.FirstName} key={item.FirstName + item.LastName}>
                            <div className="viewDetailsDiv">
                              <div className="viewdetailNameDiv">
                                <span className = "viewRelationNameDiv"> {item.FirstName} {item.LastName}</span>
                                <div className = "viewRelationDiv">
                                  <div className ="viewRelationTextDiv">
                                    {item.Relation}
                                  </div>
                                </div>
                              </div>
                              <div className="viewdetailEmailDiv">
                                <span> {item.Email} </span>
                              </div>
                              <div className="viewdetailPhoneDiv">
                                <span> {item.PhoneNo} </span>
                              </div>
                            </div>
                          </li>
                      )
              })
    }
    return HTML;
  }

  returnUnIdentifiedElements = () => {

    let HTML =[];

    if(this.state.UnidentifiedContact.length > 0)
    {
      this.state.UnidentifiedContact.map((item) => {
                HTML.push(<li className="view-list-group-item" data-category={item.FirstName} key={item.FirstName + item.LastName}>
                            <div className="viewDetailsDiv">
                              <div className="viewdetailNameDiv">
                                <span> {item.FirstName} {item.LastName} </span>
                              </div>
                              <div className="viewdetailEmailDiv">
                                <span> {item.Email} </span>
                              </div>
                              <div className="viewdetailPhoneDiv">
                                <span> {item.PhoneNo} </span>
                              </div>
                            </div>
                          </li>
                      )
              })
    }
    return HTML;
  }

  openManageContacts = () =>{
    this.props.history.push(`/contacts`)
  }

  render() {
    return (
      <div className="viewPhoneBookOuterDiv">
        <div className="viewPhoneBookHeaderDiv">
          <span> Contacts </span>
        </div>
        <div className="viewPhoneBookOuterDetailsDiv">
          <div className="filter-list">
            <form className="familyFormDiv">
              <fieldset className="view-form-group">
                <input type="text" className="view-form-control form-control-lg" placeholder="Search" onChange={this.filterList}/>
              </fieldset>
            </form>
            <div className="ManageListDiv" onClick={this.openManageContacts}> 
                <span> Manage List </span>
            </div>
            <div className="ListDiv">
              <div className="ListHeaderDiv">
                <span> Family Member ({this.state.FamilyContact.length}) </span>
              </div>
              <div className= "ListBodyDiv">
                {this.returnFamilyElements()}
              </div>
            </div>
            <div className="ListDiv">
              <div className="ListHeaderDiv">
                <span> Unidentified ({this.state.UnidentifiedContact.length}) </span>
              </div>
              <div className= "ListBodyDiv">
                {this.returnUnIdentifiedElements()}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}


export default ViewPhoneBook;