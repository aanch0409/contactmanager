import React, { Component } from 'react';
import './App.css';
import ImportContacts from './ImportContacts'
import ContactsListPage from './ContactsListPage'
import { Route , Link } from 'react-router-dom'
import { withRouter } from "react-router-dom";
import ContactsSideBar from './ContactsSideBar'
import ViewContactsSideBar from './ViewContactSideBar'
import FetchingContacts from './FetchingContacts'
import ViewPhoneBook from './ViewPhoneBook'

class Main extends Component {
  
  componentWillMount()
  {
        let element = document.getElementById("root")
        let height = window.innerHeight - 3
        let width = window.innerWidth 
        element.style.height = height +"px"
        element.style.width = width + "px"
  }

  render() {

  	const ImportContactsconst = (props) => {
      return (
        <ImportContacts 
          {...props}
        />
      );
    }

    const ContactsListconst = (props) => {
      return (
        <ContactsListPage 
          {...props}
        />
      );
    }

    const FetchingContactsconst = (props) => {
      return (
        <FetchingContacts 
          {...props}
        />
      );
    }

    const ViewPhoneBookconst = (props) => {
      return (
        <ViewPhoneBook 
          {...props}
        />
      );
    }

    return (
      <div className="Main">
            <div className="appHeader">
            	<span className = "headerText"> Hello Joanne! </span>
      		</div>
      		<div className="appBody">
      			        <Route exact path="/" render={rouecomponent => {
                        return ImportContactsconst(this.props);}} ></Route>
                        <Route exact path="/contacts" render={rouecomponent => {
                        return ContactsListconst(this.props);}} ></Route>
                        <Route exact path="/fetching" render={rouecomponent => {
                        return FetchingContactsconst(this.props);}} ></Route>
                        <Route exact path="/viewphonebook" render={rouecomponent => {
                        return ViewPhoneBookconst(this.props);}} ></Route>
      		</div>
          <ContactsSideBar />
          <ViewContactsSideBar />
      </div>
    );
  }
}

export default withRouter((Main));
