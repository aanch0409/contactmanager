import React, { Component } from 'react';
import './App.css';
import './ContactsSideBar.css';
import { compose } from 'redux';
import { withRouter } from "react-router-dom";
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import * as actionCreators from './Actions/actionCreator'
import FamilyContactList from './FamilyContactList'

class ViewContactsSideBar extends Component {

  closeSideBar = () =>{
    document.getElementById("viewcontactsSideBarOuterIdDiv").style.display = "none";
  }



  render() {

    return (
      <div className="contactsSideBarOuterDiv" id="viewcontactsSideBarOuterIdDiv">
          <div className="contactsSideBar">
            <div className="contactsSideHeader">
              <div className="contactsSideHeaderText">
                <span> Family Members ({this.props.familymember.length}) </span>
              </div>
              <div className="contactsSideHeaderCross" onClick = {this.closeSideBar}>
                <span> X </span>
              </div>
            </div>
            <div className="contactsSideBody">
              <FamilyContactList familyContact = {this.props.familymember}/>
            </div>
          </div>
      </div>
    );
  }
}

 function mapStateToProps(state)
  {
    return{
        googlecontacts : state.googlecontacts,
        facebookcontacts : state.facebookcontacts,
        localcontacts : state.localcontacts,
        unidentified : state.unidentified,
        familymember : state.familymember
    }    
  }

 function mapDispatchToProps(dispatch)
 {
    return bindActionCreators(actionCreators, dispatch)
 }

 const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
 );

export default withRouter(compose(
    withConnect
)(ViewContactsSideBar));