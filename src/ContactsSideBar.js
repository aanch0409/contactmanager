import React, { Component } from 'react';
import './App.css';
import './ContactsSideBar.css';
import { compose } from 'redux';
import { withRouter } from "react-router-dom";
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import * as actionCreators from './Actions/actionCreator'
import UnidentifiedContactList from './UnidentifiedContactList'
import SelectContact from './SelectContact'
import { Route , Link } from 'react-router-dom'

class ContactsSideBar extends Component {
  
  closeSideBar = () =>{
    document.getElementById("contactsSideBarOuterIdDiv").style.display = "none";
  }

  render() {

    return (
      <div className="contactsSideBarOuterDiv" id="contactsSideBarOuterIdDiv">
          <div className="contactsSideBar" id="contactSideBarDiv">
            <div className="contactsSideHeader">
              <div className="contactsSideHeaderText">
                <span> Unidentified ({this.props.unidentified.length}) </span>
              </div>
              <div className="contactsSideHeaderCross" onClick = {this.closeSideBar}>
                <span> X </span>
              </div>
            </div>
            <div className="contactsSideBody" id="contactSideDiv">
              <UnidentifiedContactList contactList = {this.props.unidentified} addFamily ={this.props.importfamilymenber} removeContact = {this.props.removecontact}/>
            </div>
          </div>
          <div className="contactsSelectBody" id="contactSelectDiv">
          </div>
      </div>
    );
  }
}

 function mapStateToProps(state)
  {
    return{
        googlecontacts : state.googlecontacts,
        facebookcontacts : state.facebookcontacts,
        localcontacts : state.localcontacts,
        unidentified : state.unidentified,
        familymember : state.familymember
    }    
  }

 function mapDispatchToProps(dispatch)
 {
    return bindActionCreators(actionCreators, dispatch)
 }

 const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
 );

export default withRouter(compose(
    withConnect
)(ContactsSideBar));