export function importgooglecontacts(index){
    return {
        type : 'IMPORT_GOOGLE_CONTACTS',
        index
    }
}

export function importfacebookcontacts(index){
    return {
        type : 'IMPORT_FACEBOOK_CONTACTS',
        index
    }
}

export function importlocalcontacts(index){
    return {
        type : 'IMPORT_LOCAL_CONTACTS',
        index
    }
}

export function importcontacts(ContactId , FirstName , LastName , Email , PhoneNo , Loc , AgeRange){
    return {
        type : 'IMPORT_CONTACTS',
        ContactId,
        FirstName,
        LastName,
        Email, 
        PhoneNo,
        Loc,
        AgeRange
    }
}

export function removecontact(ContactId){
    return {
        type : 'REMOVE_CONTACT',
        ContactId
    }
}

export function importfamilymenber(ContactId , FirstName , LastName , Email , PhoneNo , Loc , AgeRange , Relation){
    return {
        type : 'IMPORT_FAMILY',
        ContactId,
        FirstName,
        LastName,
        Email, 
        PhoneNo,
        Loc,
        AgeRange,
        Relation
    }
}