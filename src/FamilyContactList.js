import React, { Component } from 'react';
import './App.css';
import FaPhoneSquare from 'react-icons/lib/fa/phone-square'
import IoEmail from 'react-icons/lib/io/email'
import ReactDOM from 'react-dom';

class FamilyContactList extends Component {

  constructor(props) {
    super(props);

    this.state = {
      familyMembers : [],
      items : [],
      isFilter : false
    }

    this.flag=false;
  }

  filterList = (event) => {
    var updatedList = this.props.familyContact;
    updatedList = updatedList.filter(({ FirstName , LastName }) => (FirstName.toLowerCase()+""+LastName.toLowerCase()).search(event.target.value.toLowerCase()) !== -1)
    this.flag=true;
    
    this.setState(
      {
        items: updatedList,
        isFilter : true
      }
    );
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.state.items != nextProps.familyContact;
  }

  componentDidUpdate(){
    if(this.state.isFilter == false)
    {
      this.setState({items: this.props.familyContact})
    }
    else
    {
      this.flag=false;
    }
  }

  returnElements = () => {

    let HTML =[];

    if(this.state.items.length > 0)
    {
      this.state.items.map((item) => {
                HTML.push(<li className="list-group-item" data-category={item.FirstName} key={item.FirstName + item.LastName}>
                          <div className="detailContactDiv">
                            <div className="familyNameDiv">
                              <span> {item.FirstName} {item.LastName}</span>
                            </div>
                            <div className="detailFamilyOtherDiv">
                              <div className="detailEmailDiv">
                                <IoEmail className= "detailContactIcon"/>
                                <span> {item.Email} </span>
                              </div>
                              <div className="detailPhoneDiv">
                                <FaPhoneSquare className= "detailContactIcon"/>
                                <span> {item.PhoneNo} </span>
                              </div>
                            </div>
                          </div>
                          <div className="familyContactArrowDiv" onClick = {this.arrowClick}>
                            <span> {item.Relation} </span>
                          </div>
                        </li>
                      )
              })
    }
    else
    {
      HTML.push(<div className ="NoContacts">
                  <span> No contacts to Show </span>
                </div>
            )

    }
    return HTML;
  }

  render() {
    return (
      <div className="filter-list">
          <form className="familyFormDiv">
            <fieldset className="form-group">
              <input type="text" className="form-control form-control-lg" placeholder="Search" onChange={this.filterList}/>
            </fieldset>
          </form>
          <ul className="list-group">
            {
              this.returnElements()
            }
          </ul>
      </div>
    );
  }
}

export default FamilyContactList;