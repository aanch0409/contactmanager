function unidentified(state = {} , action)
{
    switch (action.type)
        {
        	case 'IMPORT_CONTACTS' : 
                return [ ...state,{
                ContactId : action.ContactId,
                FirstName : action.FirstName,
                LastName : action.LastName,
                Email : action.Email,
                PhoneNo : action.PhoneNo,
                Loc : action.Loc,
                AgeRange : action.AgeRange
                }]
            case 'REMOVE_CONTACT' : 
                return state.filter(({ ContactId }) => ContactId !== action.ContactId)
            default :
                return state  
        }
  
}

export default unidentified