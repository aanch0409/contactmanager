import {combineReducers} from 'redux'
import {routerReducer} from 'react-router-redux'

import facebookcontacts from './facebookcontactsReducer'
import googlecontacts from './googlecontactsReducer'
import localcontacts from './localcontactsReducer'
import unidentified from './unidentifiedReducer'
import familymember from './familymemberReducer'

const rootReducer = combineReducers (
{googlecontacts , facebookcontacts , localcontacts, unidentified , familymember , routing : routerReducer}
)

export default rootReducer;