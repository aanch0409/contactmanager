function familymember(state = {} , action)
{
    switch (action.type)
        {
        	case 'IMPORT_FAMILY' : 
                return [ ...state,{
                ContactId : action.ContactId,
                FirstName : action.FirstName,
                LastName : action.LastName,
                Email : action.Email,
                PhoneNo : action.PhoneNo,
                Loc : action.Loc,
                AgeRange : action.AgeRange,
                Relation : action.Relation
                }]
            default :
                return state  
        }
  
}

export default familymember