import React, { Component } from 'react';
import './App.css';
import DetailContactList from './DetailContactList'

class UnidentifiedContactList extends Component {

  render() {
    return (
      <div className="unidentifiedContactList">
              <div className="contactBodyText">
                <span> Select a contact and select who is to you </span>
              </div>
              <div className="contactOuterBody">
                {this.props.contactList.map((contact,i) => <DetailContactList  contact = {contact} addFamily = {this.props.addFamily} removeContact = {this.props.removeContact}/>)}
              </div>
      </div>
    );
  }
}

export default UnidentifiedContactList;
