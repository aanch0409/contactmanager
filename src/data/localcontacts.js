const localcontacts =
[
  {
    "ContactId" : "6",
    "FirstName": "Cassio",
    "LastName": "Zen",
    "Email": "cassiozen@gmail.com",
    "PhoneNo" : "9823456789",
    "Loc" : "123 Maple Street",
    "AgeRange" : "20-30"
  },
  {
    "ContactId" : "7",
    "FirstName": "Dan",
    "LastName": "Abramov",
    "Email": "gaearon@somewhere.com",
    "PhoneNo" : "9845456789",
    "Loc" : "456 Oak Lane",
    "AgeRange" : "30-40"
  },
  {
    "ContactId" : "8",
    "FirstName": "Pete",
    "LastName": "Hunt",
    "Email": "floydophone@somewhere.com",
    "PhoneNo" : "9823456789",
    "Loc" : "1234 Peach Drive",
    "AgeRange" : ""
  },
  {
    "ContactId" : "9",
    "FirstName": "Paul",
    "LastName": "O’Shannessy",
    "Email": "zpao@somewhere.com",
    "PhoneNo" : "8453523455",
    "Loc" : "351 White Paradise",
    "AgeRange" : "50-60"
  },
  {
    "ContactId" : "10",
    "FirstName": "Ryan",
    "LastName": "Florence",
    "Email": "rpflorence@somewhere.com",
    "PhoneNo" : "9153523455",
    "Loc" : "Seattle WA 98052",
    "AgeRange" : "0-10"
  },
  {
    "ContactId" : "11",
    "FirstName": "Sebastian",
    "LastName": "Markbage",
    "Email": "sebmarkbage@here.com",
    "PhoneNo" : "8453523455",
    "Loc" : "20341 Whitworth",
    "AgeRange" : "10-20"
  }
]

export default localcontacts