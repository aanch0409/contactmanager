const googlecontacts =
[
  {
    "ContactId" : "12",
    "FirstName": "Aanchal",
    "LastName": "Malhotra",
    "Email": "aanchal@gmail.com",
    "PhoneNo" : "9167823455",
    "Loc" : "Shalimar Bagh,Delhi",
    "AgeRange" : "20-30"
  },
  {
    "ContactId" : "13",
    "FirstName": "Rishi",
    "LastName": "Gupta",
    "Email": "rishi@somewhere.com",
    "PhoneNo" : "9239823455",
    "Loc" : "Pitampura,Delhi",
    "AgeRange" : "40-50"
  },
  {
    "ContactId" : "14",
    "FirstName": "Nisha",
    "LastName": "Sharma",
    "Email": "nisha@somewhere.com",
    "PhoneNo" : "8457823455",
    "Loc" : "Kapil Vihar,Delhi",
    "AgeRange" : "30-40"
  },
  {
    "ContactId" : "16",
    "FirstName": "Parveen",
    "LastName": "Jain",
    "Email": "parveen@somewhere.com",
    "PhoneNo" : "8567823455",
    "Loc" : "Rohini,Delhi",
    "AgeRange" : "20-30"
  },
  {
    "ContactId" : "17",
    "FirstName": "Vihaan",
    "LastName": "Wig",
    "Email": "vihaan@somewhere.com",
    "PhoneNo" : "8453523455",
    "Loc" : "Karol Bagh,Delhi",
    "AgeRange" : "10-20"
  },
  {
    "ContactId" : "18",
    "FirstName": "Aarohi",
    "LastName": "Singh",
    "Email": "aarohi@here.com",
    "PhoneNo" : "8486523455",
    "Loc" : "NSEZ,Noida",
    "AgeRange" : "0-10"
  }
]

export default googlecontacts