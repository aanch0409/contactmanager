const facebookcontacts =
[
  {
    "ContactId" : "0",
    "FirstName": "Romin",
    "LastName": "Irani",
    "Email": "Romin@gmail.com",
    "PhoneNo" : "408-1234567",
    "Loc" : "",
    "AgeRange" : "10-20"
  },
  {
    "ContactId" : "1",
    "FirstName": "Dan",
    "LastName": "Abramov",
    "Email": "neilrirani@gmail.com",
    "PhoneNo" : "408-1111111",
    "Loc" : "3102 Highway",
    "AgeRange" : "10-20"
  },
  {
    "ContactId" : "2",
    "FirstName": "Tom",
    "LastName": "Hanks",
    "Email": "tomhanks@gmail.com",
    "PhoneNo" : "408-2222222",
    "Loc" : "Philadelphia, PA",
    "AgeRange" : "20-30"
  },
  {
    "ContactId" : "3",
    "FirstName": "Paul",
    "LastName": "O’Shannessy",
    "Email": "zpao@somewhere.com",
    "PhoneNo" : "405 Whitworth",
    "Loc" : "405 Whitworth",
    "AgeRange" : "30-40"
  },
  {
    "ContactId" : "4",
    "FirstName": "Ryan",
    "LastName": "Florence",
    "Email": "rpflorence@somewhere.com",
    "PhoneNo" : "850-648-4200",
    "Loc" : "Mexico Beach, FL",
    "AgeRange" : "40-50"
  },
  {
    "ContactId" : "5",
    "FirstName": "Sebastian",
    "LastName": "Markbage",
    "Email": "sebmarkbage@here.com",
    "PhoneNo" : "850-648-4220",
    "Loc" : "Philadelphia 76ers",
    "AgeRange" : "20-30"
  }
]

export default facebookcontacts