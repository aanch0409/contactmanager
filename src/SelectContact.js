import React, { Component } from 'react';
import './App.css';
import ReactDOM from 'react-dom';
import TiLocation from 'react-icons/lib/ti/location';
import { compose } from 'redux';
import { withRouter } from "react-router-dom";
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import * as actionCreators from './Actions/actionCreator'
import IoChevronLeft from 'react-icons/lib/io/chevron-left'

class SelectContact extends Component {

  closeSideBar = () =>{    
    let container =document.getElementById('contactSelectDiv');
    ReactDOM.unmountComponentAtNode(container);
    document.getElementById("contactSideBarDiv").style.display = "flex";
  }

  addFamily = () =>{
  	let Relation = document.getElementsByClassName('RelationGreenDiv')[0].parentElement.getElementsByClassName("RelationBodyTextDiv")[0].children[0].innerText
  	this.props.addFamily(this.props.contact.ContactId , this.props.contact.FirstName , this.props.contact.LastName , this.props.contact.Email , this.props.contact.PhoneNo , this.props.contact.Loc , this.props.contact.AgeRange , Relation);
  	this.props.removeContact(this.props.contact.ContactId);
  	let container =document.getElementById('contactSelectDiv');
    ReactDOM.unmountComponentAtNode(container);
    document.getElementById("contactSideBarDiv").style.display = "flex";
    document.getElementById("contactsSideBarOuterIdDiv").style.display = "none";
  }

  markSelected = (e) =>{  	
  	let key = e.currentTarget.id;
  	let containers = document.getElementsByClassName('RelationBodyDiv');
  	let i =0;
  	while(i< containers.length)
    {
    	containers[i].style.border = "none";
    	containers[i].children[0].classList.remove("RelationGreenDiv");
    	i++;
    }

  	let KeyElement = document.getElementById(key)
  	let GreyDiv = KeyElement.getElementsByClassName("RelationGreyDiv")[0];
  	if(!(GreyDiv.classList.contains("RelationGreenDiv")))
  	{
	  	document.getElementById("RelationBodySelectButtonDivId").style.display = "flex";
	  	GreyDiv.classList.add("RelationGreenDiv");
	  	KeyElement.style.border = "1px solid #00BE6E"
	}
	else
	{
		document.getElementById("RelationBodySelectButtonDivId").style.display = "none";
	  	GreyDiv.classList.remove("RelationGreenDiv");
	  	KeyElement.style.border = "none"
	}

  }

  render() {

    return (
      <div className="selectcontactsSideBarOuterDiv" id="selectcontactsSideBarOuterIdDiv">
          <div className="contactsSideBar">
            <div className="contactsSideHeader">
              <div className="contactsSelectSideHeaderCross" onClick = {this.closeSideBar}>
                <IoChevronLeft className="moveLeft"/>
              </div>
              <div className="contactsSelectSideHeaderText">
                <span> {this.props.contact.FirstName} {this.props.contact.LastName}</span>
              </div>
            </div>
            <div className="contactsSideBody">
            	<div className="contactsSelectBodyHeader">
            		<div className="contactsSelectBodyHeaderDiv">
	            		<div className="selectNameDiv">
	              			<span> Who is {this.props.contact.FirstName} {this.props.contact.LastName} to you? </span>
	            		</div>
	            		<div className="selectOtherDiv">
	                		<div className="detailAgeDiv">
	                  			<div className="ageTextDiv">AGE</div>
	                  			<span> {this.props.contact.AgeRange} </span>
	                		</div>
	                		<div className="detailLocDiv">
	                  			<TiLocation className= "detailLocIcon"/>
	                  			<span> {this.props.contact.Loc} </span>
	                		</div>
	            		</div>
	            	</div>
            	</div>
            	<div className="selectRelationTypeDiv">
            		<div className="ChooseRelationTextDiv">
	              		<span> Select one of them to help us identify the correct family member </span>
	            	</div>
	            	<div className="RelationBodyOuterDiv">
	            		<div className="RelationBodyDiv" id="RelationFatherDiv"  onClick={this.markSelected}>
	            			<div className="RelationGreyDiv">
	            			</div>
	            			<div className="RelationBodyTextDiv" >
	            				<span> Father </span>
	            			</div>
	            		</div>
	            		<div className="RelationBodyDiv" id="RelationMotherDiv"  onClick={this.markSelected}>
	            			<div className="RelationGreyDiv">
	            			</div>
	            			<div className="RelationBodyTextDiv">
	            				<span> Mother </span>
	            			</div>
	            		</div>
	            		<div className="RelationBodyDiv" id="RelationSisterDiv"  onClick={this.markSelected}>
	            			<div className="RelationGreyDiv">
	            			</div>
	            			<div className="RelationBodyTextDiv">
	            				<span> Sister </span>
	            			</div>
	            		</div>
	            		<div className="RelationBodyDiv" id="RelationBrotherDiv" onClick={this.markSelected}>
	            		    <div className="RelationGreyDiv">
	            			</div>
	            			<div className="RelationBodyTextDiv">
	            				<span> Brother </span>
	            			</div>
	            		</div>
	            		<div className="RelationBodySelectButton" >
	            			<div className="RelationBodySelectButtonDiv" id="RelationBodySelectButtonDivId" onClick={this.addFamily}>
	            				<span> Select </span>
	            			</div>
	            		</div>
	            	</div>
            	</div>
            </div>
          </div>
      </div>
    );

	}
}

 function mapStateToProps(state)
  {
    return{
        googlecontacts : state.googlecontacts,
        facebookcontacts : state.facebookcontacts,
        localcontacts : state.localcontacts,
        unidentified : state.unidentified,
        familymember : state.familymember
    }    
  }


export default SelectContact;