import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import * as actionCreators from './Actions/actionCreator'

import Main from './Main'

function mapStateToProps(state)
{
    return{
        googlecontacts : state.googlecontacts,
        facebookcontacts : state.facebookcontacts,
        localcontacts : state.localcontacts,
        unidentified : state.unidentified,
        familymember : state.familymember
    }    
}

function mapDispatchToProps(dispatch)
{
    return bindActionCreators(actionCreators, dispatch)
}

const App = connect(mapStateToProps , 
                   mapDispatchToProps)(Main)

export default App;
