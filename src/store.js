import {createStore} from 'redux'
import {syncHistoryWithStore} from 'react-router-redux'
import { withRouter } from "react-router-dom";
import { compose } from 'redux';

import rootReducer from './Reducer/rootReducer.js'

import localcontacts from './data/localcontacts.js'

import facebookcontacts from './data/facebookcontacts.js'

import googlecontacts from './data/googlecontacts.js'

const defaultState = {
    googlecontacts : googlecontacts,
    facebookcontacts : facebookcontacts,
    localcontacts : localcontacts,
    unidentified : [],
    familymember : []
}

const store = createStore(rootReducer , defaultState)

export default withRouter(store)