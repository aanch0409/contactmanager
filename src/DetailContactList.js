import React, { Component } from 'react';
import './App.css';
import FaPhoneSquare from 'react-icons/lib/fa/phone-square'
import IoEmail from 'react-icons/lib/io/email'
import FaArrowRight from 'react-icons/lib/fa/arrow-right'
import SelectContact from './SelectContact'
import ReactDOM from 'react-dom';

class DetailContactList extends Component {

  arrowClick = (e) => {
      e.preventDefault()
      e.stopPropagation();
      e.nativeEvent.stopImmediatePropagation();
      document.getElementById("contactSideBarDiv").style.display = "none";
      let eventcontainer = document.getElementById('contactSelectDiv'); 
      ReactDOM.render(<SelectContact contact = {this.props.contact} addFamily = {this.props.addFamily} removeContact = {this.props.removeContact}/>, eventcontainer);
  }

  render() {

    return (
        <div className="detailContactOuterDiv">
          <div className="detailContactDiv">
            <div className="detailNameDiv">
              <span> {this.props.contact.FirstName} {this.props.contact.LastName}</span>
            </div>
            <div className="detailOtherDiv">
                <div className="detailEmailDiv">
                  <IoEmail className= "detailContactIcon"/>
                  <span> {this.props.contact.Email} </span>
                </div>
                <div className="detailPhoneDiv">
                  <FaPhoneSquare className= "detailContactIcon"/>
                  <span> {this.props.contact.PhoneNo} </span>
                </div>
            </div>
          </div>
          <div className="detailContactArrowDiv" id = {this.props.contact.ContactId} onClick = {this.arrowClick}>
              <FaArrowRight className= "detailArrowIcon"/>
          </div>
        </div>
    );
  }
}

export default DetailContactList;