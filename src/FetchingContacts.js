import React, { Component } from 'react';
import './App.css';
import FaCircleONotch from 'react-icons/lib/fa/circle-o-notch';

class FetchingContacts extends Component {

  componentDidMount(){
    setTimeout(() => {
      this.props.history.push(`/contacts`)
    },1000)
  }

  render() {
    return (
      <div className="importContactsBody">
      	  <div className="circleDiv">
      	   	<ul>
  				    <li className="semiActive"></li>
  				    <li className="nonActive"></li>
				    </ul>  
      		</div>
      		<div className="popUpheaderText">
      			<span className="importSpan"> Import your contacts </span>
      			<span className="helpSpan"> Help? </span>
      		</div>
      		<div className="popUpDivArea">
	      		<div className="popUpDiv">
	      			  <div className="popUpHeader">
	      			  	<span className="popUpHeaderSpan"> Fetching Contacts... </span>
	      			  </div>
	      			  <div className="popUpBody">
                  <div className="spinnerDiv">
                    <FaCircleONotch className ="spinner"/>
	      			    </div>
                </div>
	      		</div>
	      	</div>
      </div>
    );
  }
}

export default FetchingContacts;
